import React, {useEffect} from 'react';
import {useNavigate} from "react-router-dom";
import {useAppDispatch, useAppSelector} from "../hooks/redux";
import {RootState} from "../store/store";
import {userSlice} from "../store/reducers/userReducer";
import {onAuthStateChanged} from "firebase/auth";
import {auth} from "../config/firebase-config";
import {User as FirebaseUser} from "@firebase/auth";

const CheckAuth: (children: { children: any }) => (JSX.Element) = ({children}) => {

//const CheckAuth = (Children:React.ComponentType) => {

    const {user} = useAppSelector((state: RootState) => state.userReducer)
    const {setUserState} = userSlice.actions
    const dispatch = useAppDispatch()
    const history = useNavigate()

    useEffect( ()=> {
        if (user.email == '') {
            onAuthStateChanged(auth, async (currentUser: FirebaseUser | null) => {
                if (currentUser) {
                    const userState = {
                        email: currentUser.email,
                        displayName: currentUser.displayName,
                        photoURL: currentUser.photoURL
                    }
                    await dispatch(setUserState(userState))
                    console.log(currentUser)
                } else {
                    console.log(currentUser)
                    history('/auth')
                }
            })
        }

    }, [])

    return children;
};

export default CheckAuth;