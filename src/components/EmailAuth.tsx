import React, {FC, useState} from 'react';
import {Box, Button, Divider, TextField} from "@mui/material";
import {signInWithEmailAndPassword} from "firebase/auth";
import {auth} from "../config/firebase-config";
import { useSelector, useDispatch } from 'react-redux'
import {applicationSlice} from '../store/reducers/applicationSlice'
import {useAppDispatch, useAppSelector} from "../hooks/redux";
import {RootState} from "../store/store";
import {useNavigate} from "react-router-dom";

const EmailAuth: FC = () => {

    const {formType} = useAppSelector((state:RootState) => state.applicationReducer)
    const {setFormType} = applicationSlice.actions;
    const dispatch = useAppDispatch()
    const [loginEmail, setLoginEmail] = useState<string>('')
    const [loginPassword, setLoginPassword] = useState<string>('')
    const history = useNavigate()

    const handleChangeFormType = (type: string) => {
        dispatch(setFormType(type))
    }

    const login = async () => {
        try{
            const user = await signInWithEmailAndPassword(auth, loginEmail, loginPassword)
            history('/')
            console.log(user)
        } catch (error:any) {
            console.log(error.message)
        }
    }

    return (
        <div>
            <Box sx={{color: 'primary.main', paddingLeft: '10px'}}>
                <h3>
                    Авторизация
                </h3>
            </Box>
            <Divider/>
            <Box sx={{color: 'primary.main', padding: '10px'}}>
                <TextField
                    fullWidth
                    label="E-mail"
                    variant="outlined"
                    value={loginEmail}
                    onChange={(event) => setLoginEmail(event.target.value)}/>
            </Box>
            <Box sx={{color: 'primary.main', padding: '10px', textAlign: 'center'}}>
                <TextField
                    fullWidth
                    label="Password"
                    variant="outlined"
                    type="password"
                    value={loginPassword}
                    onChange={(event) => setLoginPassword(event.target.value)}/>
            </Box>
            <Box sx={{color: 'primary.main', padding: '10px', textAlign: 'center'}}>
                <Button onClick={login} sx={{color: '#fff'}} variant="contained">Войти</Button>
            </Box>
            <Box onClick={() => handleChangeFormType('register')}
                 sx={{
                     padding: '10px',
                     textAlign: 'center',
                     color: 'primary.main',
                     cursor: 'pointer',
                     '&:hover': {
                         color: 'primary.light'
                     }
                 }}>
                <span>Нет аккаунта? Регистрация</span>
            </Box>
        </div>
    );
};

export default EmailAuth;