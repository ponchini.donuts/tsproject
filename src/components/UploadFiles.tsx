import {getDownloadURL, ref, uploadBytesResumable } from 'firebase/storage';
import {storage} from "../config/firebase-config";
import React, {useState} from 'react';
import Dropzone from 'react-dropzone';

function Bid(){
    throw new Error('Bid hilla');
}

const UploadFiles = () => {

    const [bid, setBid] = useState<boolean>(false)

    const drag:React.CSSProperties = {
        border: '2px dashed #eee',
        margin: '10px 0 0 0'
    }

    const thumb:React.CSSProperties = {
        display: 'inline-flex',
        borderRadius: 2,
        border: '1px solid #eaeaea',
        marginBottom: 8,
        marginRight: 8,
        width: 100,
        height: 100,
        padding: 4,
        boxSizing: 'border-box'
    };

    const thumbInner:React.CSSProperties = {
        display: 'flex',
        minWidth: 0,
        overflow: 'hidden'
    };

    const img:React.CSSProperties = {
        display: 'block',
        width: 'auto',
        height: '100%'
    };

    const [file, setFile] = useState<File>();
    const [url, setUrl] = useState<string>();
    const [progress, setProgress] = useState<number>(0)

    const uploadFiles = (file: File) => {
        if(!file) return;
        const storageRef = ref(storage, `/files/${file.name}`)
        const uploadTask = uploadBytesResumable(storageRef, file);
        uploadTask.on("state_changed", (snapshot) => {
            const progressPercent = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
            setProgress(progressPercent)
        }, (err)=> console.log(err),
            () => {
            getDownloadURL(uploadTask.snapshot.ref)
                .then(url => console.log(url))
            })
    }

    const handleUpload = () => {
        if(file){
            uploadFiles(file)
        } else {
            setBid(true)
        }
        console.log(file)
    }

    return (
        <div>
            {bid ? Bid() : null}
            <Dropzone onDrop={acceptedFiles => {
                console.log(acceptedFiles)
                setFile(acceptedFiles[0])
                setUrl(URL.createObjectURL(acceptedFiles[0]))
            }}>
                {({getRootProps, getInputProps}) => (
                    <section style={drag}>
                        <div {...getRootProps()}>
                            <input {...getInputProps()} />
                            <p style={{textAlign: 'center', marginBlockStart: 'auto', marginBlockEnd: 'auto', height: '60px'}}>Перетащите сюда файлы, или кликните, чтобы выбрать</p>
                        </div>
                    </section>
                )}
            </Dropzone>
            <button onClick={handleUpload}>Upload</button>
            {url ?
                (
                    <div style={thumb}>
                        <div style={thumbInner}>
                            <img
                                src={url}
                                style={img}
                            />
                        </div>
                    </div>
                )
                :
                ''
            }
        </div>
    );
};

export default UploadFiles;