import React, {ComponentType} from 'react';
import {FallbackProps} from "react-error-boundary";

const ErrorFallback:ComponentType<FallbackProps> = ({error, resetErrorBoundary}) => {
    return (
        <div>
            <p>Something went wrong</p>
            <pre>{error.message}</pre>
            <button onClick={resetErrorBoundary}>Try again</button>
        </div>
    );
};

export default ErrorFallback;