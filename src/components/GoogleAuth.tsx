import React, {FC} from 'react';
import {Box, Grid} from "@mui/material";
import {GoogleAuthProvider, signInWithPopup} from "firebase/auth";
import {auth} from "../config/firebase-config";
import {useNavigate} from "react-router-dom";

const GoogleAuth: FC = () => {

    const provider = new GoogleAuthProvider();
    const history = useNavigate()

    const handleGoogleAuth = async () => {

        try {
            const result = await signInWithPopup(auth, provider)
            const credential = GoogleAuthProvider.credentialFromResult(result);
            const token = credential?.accessToken;
            // The signed-in user info.
            const user = result.user;
            history('/')
        } catch (error:any) {
            const errorCode = error.code;
            const errorMessage = error.message;
            // The email of the user's account used.
            const email = error.email;
            // The AuthCredential type that was used.
            const credential = GoogleAuthProvider.credentialFromError(error);
        }
    }

    return (
        <Box sx={{
            flexGrow: 1,
            verticalAlign: 'middle',
            padding: '10px',
            cursor: 'pointer',
            '&:hover': {
                backgroundColor: 'primary.light'
            }
        }}>
            <Grid onClick={handleGoogleAuth} container justifyContent='center' spacing={1}>
                <Grid item>
                    <img src={require('../pictures/google-logo.png')} alt='' width='20px'/>
                </Grid>
                <Grid item>
                    Войти через Google
                </Grid>
            </Grid>
        </Box>
    );
};

export default GoogleAuth;