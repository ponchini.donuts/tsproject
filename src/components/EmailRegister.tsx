import React, {FC, useState} from 'react';
import {Box, Button, Divider, TextField} from "@mui/material";
import {useAppDispatch} from "../hooks/redux";
import {applicationSlice} from "../store/reducers/applicationSlice";
import {createUserWithEmailAndPassword} from "firebase/auth";
import {auth} from "../config/firebase-config";
import {useNavigate} from "react-router-dom";

const EmailRegister: FC = () => {

    const [registerEmail, setRegisterEmail] = useState<string>('')
    const [registerPassword, setRegisterPassword] = useState<string>('')
    const history = useNavigate()

    const {setFormType} = applicationSlice.actions;
    const dispatch = useAppDispatch()

    const handleChangeFormType = (type: string):void => {
        dispatch(setFormType(type))
    }

    const register = async () => {
        try{
            const user = await createUserWithEmailAndPassword(auth, registerEmail, registerPassword)
            history('/')
            console.log(user)
        } catch (error:any) {
            console.log(error.message)
        }
    }

    return (
        <div>
            <Box sx={{color: 'primary.main', paddingLeft: '10px'}}>
                <h3>
                    Регистрация
                </h3>
            </Box>
            <Divider/>
            <Box sx={{color: 'primary.main', padding: '10px'}}>
                <TextField
                    fullWidth
                    label="E-mail"
                    variant="outlined"
                    value={registerEmail}
                    onChange={(event) => setRegisterEmail(event.target.value)}/>
            </Box>
            <Box sx={{color: 'primary.main', padding: '10px', textAlign: 'center'}}>
                <TextField
                    fullWidth
                    label="Password"
                    variant="outlined"
                    type="password"
                    value={registerPassword}
                    onChange={(event) => setRegisterPassword(event.target.value)}/>
            </Box>
            <Box sx={{color: 'primary.main', padding: '10px', textAlign: 'center'}}>
                <Button onClick={register} sx={{color: '#fff'}} variant="contained">Регистрация</Button>
            </Box>
            <Box onClick={() => handleChangeFormType('login')}
                 sx={{
                     padding: '10px',
                     textAlign: 'center',
                     color: 'primary.main',
                     cursor: 'pointer',
                     '&:hover': {
                         color: 'primary.light'
                     }
                 }}>
                <span>Есть аккаунт? Войти</span>
            </Box>
        </div>
    );
};

export default EmailRegister;