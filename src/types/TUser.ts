export interface TUser {
    displayName: string | null,
    email: string | null,
    photoURL: string | null,
}