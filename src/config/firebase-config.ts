// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getAuth} from "firebase/auth";
import {getStorage} from "firebase/storage"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDF5jEJohd-wGh1o91ERkWwQe3DhsBx21I",
    authDomain: "tsproject-125db.firebaseapp.com",
    projectId: "tsproject-125db",
    storageBucket: "tsproject-125db.appspot.com",
    messagingSenderId: "344155417152",
    appId: "1:344155417152:web:994fe07dd8e0332781cda5",
    measurementId: "G-2DSLH9NC4C"
};

// Initialize Firebase
export  const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const storage = getStorage(app);
