import React, {FC} from 'react';
import {Container, Divider, Paper} from "@mui/material";
import EmailAuth from "../components/EmailAuth";
import {useAppSelector} from "../hooks/redux";
import {RootState} from "../store/store";
import EmailRegister from "../components/EmailRegister";
import GoogleAuth from "../components/GoogleAuth";

const Auth: FC = () => {

    const {formType} = useAppSelector((state: RootState) => state.applicationReducer)

    return (
        <Container maxWidth={"md"} sx={{height: '100vh', alignItems: 'center', display: 'flex'}}>
            <Paper elevation={3} sx={{margin: '0 auto', width: '50%'}}>
                {formType === 'login' ?
                    (
                        <EmailAuth/>
                    ) :
                    (
                        <EmailRegister/>
                    )}
                <Divider/>
                <GoogleAuth/>
            </Paper>
        </Container>
    )
}

export default Auth