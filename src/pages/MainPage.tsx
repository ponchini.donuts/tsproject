import React from 'react';
import {Link} from "react-router-dom";
import {signOut} from "firebase/auth";
import {auth} from "../config/firebase-config";
import {AppBar, Box, Container, Toolbar, Typography} from "@mui/material";
import UploadFiles from "../components/UploadFiles";

const MainPage = () => {

    const logout = async () => {
        console.log('logging out')
        await signOut(auth)
    }

    return (
        <div>
            <AppBar position='sticky' color='primary' sx={{alignItems: 'center', display: 'inline-flex'}}>
                <Container>
                    <Toolbar>
                        <Typography
                            sx={{width: '50%'}}
                            color='#fff'
                            variant="h6">
                            Cloud Storage
                        </Typography>
                        <Box sx={{flexGrow: 1, width: '50%', textAlign: 'right'}}>
                            text
                        </Box>
                    </Toolbar>
                </Container>
            </AppBar>
            <Container sx={{alignItems: 'center', display: 'block'}}>
                <UploadFiles/>
                <Box sx={{padding: '24px'}}>
                    <div>nice</div>
                    <Link to='/auth'>auth</Link>
                    <button onClick={logout}>logout</button>
                </Box>
            </Container>
        </div>
    );
};

export default MainPage;