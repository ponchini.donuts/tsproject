import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {createTheme, CssBaseline, ThemeProvider} from "@mui/material";
import {Provider} from "react-redux";
import {setupStore} from './store/store'

const store = setupStore();
const theme = createTheme({
    components: {
        MuiCssBaseline: {
            styleOverrides: {
                body: {
                    color: '#9f9f9f'
                }
            }
        }
    },
    palette: {
        primary: {
            main: '#42a5f5',
            light: '#9cd3ff'
        },
    }
});

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <CssBaseline/>
                <App/>
            </ThemeProvider>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
)
;

