import {createSlice, PayloadAction} from '@reduxjs/toolkit'

interface applicationState {
    formType: string;
}

const initialState: applicationState = {
    formType: 'login'
}

export const applicationSlice = createSlice({
    name: 'application',
    initialState,
    reducers: {
        setFormType(state, action:PayloadAction<string>) {
            state.formType = action.payload
        }
    },
})

export default applicationSlice.reducer