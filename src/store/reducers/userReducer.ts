import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {TUser} from "../../types/TUser";

interface userState {
    user: TUser
}

const initialState: userState = {
    user: {
        email: '',
        displayName: '',
        photoURL: ''
    }
}

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setUserState(state, action:PayloadAction<TUser>) {
            state.user = action.payload
        }
    },
})

export default userSlice.reducer