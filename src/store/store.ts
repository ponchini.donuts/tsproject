import { configureStore, combineReducers } from '@reduxjs/toolkit'
import applicationReducer from './reducers/applicationSlice'
import userReducer  from "./reducers/userReducer";

const rootReducer = combineReducers({
    applicationReducer,
    userReducer
})

export const setupStore = () => configureStore({
    reducer: rootReducer
})
export type RootState = ReturnType<typeof rootReducer>
export type AppStore = ReturnType<typeof setupStore>
export type AppDispatch = AppStore['dispatch']

export default setupStore()