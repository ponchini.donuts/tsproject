import React from 'react';
import {BrowserRouter, Route, Routes,} from "react-router-dom";
import Auth from "./pages/Auth";
import CheckAuth from "./hoc/CheckAuth";
import MainPage from "./pages/MainPage";
import {ErrorBoundary} from "react-error-boundary";
import ErrorFallback from "./components/ErrorFallback";

function App() {
    return (
        <ErrorBoundary
            FallbackComponent={ErrorFallback}>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={
                        <CheckAuth>
                            <MainPage/>
                        </CheckAuth>
                    }/>
                    <Route path='/auth' element={<Auth/>}/>
                </Routes>
            </BrowserRouter>
        </ErrorBoundary>
    )
        ;
}

export default App;
